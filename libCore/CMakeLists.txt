
set(Target ONICore)

SET(SRCs
   image.h
   image.hxx
   image-inst.cpp
   point.h
   point.cpp
   rect.h
   image_helper.h

)


oni_add_library(${Target} ${SRCs})

# Unit_test DIR
#add_subdirectory( tests )
