#pragma once

#include "image.h"
#include "img_proc.h"
#include <algorithm>
#include <functional>
#include <iostream>
#include <unordered_set>

namespace oni {

// This function return all object found in image 
// A pixel belong to Object if his value > threashold 
std::vector<std::vector<Point>> ONI_IMAGEPROCESSING_EXPORT
segmentObjectContour(const Image32FC1::pointer &a_input, const float a_threshold);

Rect ONI_IMAGEPROCESSING_EXPORT contourToRect(const std::vector<Point> &a_contour);

class ONI_IMAGEPROCESSING_EXPORT MoleculesFinder
{
public:
    std::vector<Rect> operator()(const typename Image32FC1::pointer a_input)
    {
        auto l_input = Image32FC1::create(a_input->dimensions());
        std::copy(a_input->begin(), a_input->end(), l_input->begin());

        Image32FC1::pointer l_preProcessImg;
        preprocessing(l_input, l_preProcessImg);

        return findCell(l_preProcessImg);
    }

private:
    // a_output is passed by ref so we can change the pointer
    inline void preprocessing(const Image32FC1::pointer a_input, Image32FC1::pointer &a_output)
    {
        // Template type deducing should work ....
        const auto l_background = oni::blur<float>(a_input, Size(63,63));
        const auto l_diff_img = oni::substract<float>(a_input, l_background);
        auto l_eroded_img = oni::erode<float>(l_diff_img, Size(3, 3));

        // Threashold
        oni::binary_thresold_Inplace<float>(l_eroded_img, 2, 0, 255);
        a_output = oni::dilate<float>(l_eroded_img, Size(3, 3));
      //  a_output = l_eroded_img; // juste a copy of pointer
    }
    inline std::vector<Rect> findCell(const Image32FC1::pointer a_input)
    {
        std::vector<Rect> l_output_box;
        const size_t l_border_size(1);
        // border of 1 px (up,down,left,right) filled with 0
        const auto l_copyBorder = oni::copyMakeBorder<float>(a_input, l_border_size, 0);
        // Image if binary 0/255 so a threashold of 128 is fine
        const auto contours = oni::segmentObjectContour(l_copyBorder, 128);
        
        // Create Rect from each list of points 
        for (const auto &l_it : contours) {
            l_output_box.push_back(oni::contourToRect(l_it));
        }
        // Offset Rect Position according to extra borderSize
        for (auto &l_it : l_output_box) {
            l_it.offset(-1*l_border_size, -1*l_border_size);
        }


        return l_output_box;
    }
};

} // namespace oni
