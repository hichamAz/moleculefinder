#include "img_segmentation.h"

namespace oni {

    std::vector<std::vector<Point>> segmentObjectContour(const Image32FC1::pointer& a_input, const float a_threshold) // help to define Black and White
    {
        std::vector<std::vector<Point>> l_res;

        bool l_inside(false);
        std::unordered_set<Point> l_ref; // tradeoff space -time complexity
        // we can also copy image and fill it with 0 

        auto ref_contains = [&l_ref](const Point& l_pt) {
            return l_ref.find(l_pt) != l_ref.end();
        };

        Point l_pos(0, 0);
        for (size_t y = 0; y < a_input->height(); y++)
        {
            for (size_t x = 0; x < a_input->width(); x++)
            {
                l_pos = Point(x, y);
                const auto l_pixelValue = a_input->at(l_pos);
                // Scan for white pixel
                if (ref_contains(l_pos) && !l_inside)		// Entering an already discovered border
                {
                    l_inside = true;
                }
                else if (l_pixelValue > a_threshold && l_inside)	// Already discovered border point
                {
                    continue;
                }
                else if (l_pixelValue <= a_threshold && l_inside)	// Leaving a border
                {
                    l_inside = false;
                }
                else if (l_pixelValue > a_threshold && !l_inside)	// Undiscovered border point
                {
                    l_ref.insert(l_pos);
                    int checkLocationNr = 1;	// The neighbor number of the location we want to check for a new border point
                    Point checkPosition(0, 0);
                    int newCheckLocationNr; 	// Variable that holds the neighborhood position we want to check if we find a new border at checkLocationNr
                    auto startPos = l_pos;			// Set start position
                    int counter = 0; 			// Counter is used for the jacobi stop criterion
                    int counter2 = 0; 			// Counter2 is used to determine if the point we have discovered is one single point

                    // Defines the neighborhood offset position from current position and the neighborhood
                    // position we want to check next if we find a new border at checkLocationNr
                    static const std::vector<std::pair<Point, int>> l_neighborhood = {
                        {Point(-1, 0), 7},
                        {Point(-1, -1), 7},
                        {Point(0, -1), 1},
                        {Point(1, -1), 1},
                        {Point(1, 0), 3},
                        {Point(1, 1), 3},
                        {Point(0, 1), 5},
                        {Point(-1, 1), 5},
                    };

                    std::vector<Point> l_subRes;
                    // push first pixel
                    l_subRes.push_back(l_pos);

                    // Trace around the neighborhood
                    while (true)
                    {
                        checkPosition = l_pos + l_neighborhood[checkLocationNr - 1].first;
                        newCheckLocationNr = l_neighborhood[checkLocationNr - 1].second;

                        if (a_input->at(checkPosition) > a_threshold) // Next border point found
                        {
                            if (checkPosition == startPos)
                            {
                                counter++;
                                // Stopping criterion (jacob)
                                if (newCheckLocationNr == 1 || counter >= 3)
                                {
                                    // Close loop
                                    l_inside = true; // Since we are starting the search at were we first started we must set inside to true
                                    break;
                                }
                            }

                            checkLocationNr = newCheckLocationNr; // Update which neighborhood position we should check next
                            l_pos = checkPosition;
                            counter2 = 0; 						// Reset the counter that keeps track of how many neighbors we have visited
                            l_ref.insert(l_pos);
                            l_subRes.push_back(l_pos);
                        }
                        else
                        {
                            // Rotate clockwise in the neighborhood
                            checkLocationNr = 1 + (checkLocationNr % 8);
                            if (counter2 > 8)
                            {
                                // If counter2 is above 8 we have traced around the neighborhood and
                                // therefor the border is a single black pixel and we can exit
                                counter2 = 0;
                                break;
                            }
                            else
                            {
                                counter2++;
                            }
                        }
                    }

                    l_res.push_back(l_subRes);
                }
            }
        }


        return l_res;
    }
   
    
    Rect contourToRect(const std::vector<Point>& a_contour) {
        const auto [minX, maxX] = std::minmax_element(a_contour.begin(), a_contour.end(),
            [](auto const& lhs, auto const& rhs) {return lhs.x < rhs.x; });

        const auto [minY, maxY] = std::minmax_element(a_contour.begin(), a_contour.end(),
            [](auto const& lhs, auto const& rhs) {return lhs.y < rhs.y; });
        auto a_tl = Point(minX->x, minY->y);
        auto a_br = Point(maxX->x, maxY->y);
        return Rect(a_tl, a_br);
    }
}
