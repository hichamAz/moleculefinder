#pragma once

#include <iostream>
#include <algorithm>
#include <functional>
#include <opencv2/opencv.hpp>
#include "image.hxx"

namespace oni {

    std::vector<Image32FC1::pointer> ONI_IMAGEPROCESSING_EXPORT imread_32F(const std::string& a_filename);


}


