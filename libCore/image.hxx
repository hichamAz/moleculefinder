#include "image.h"

namespace oni {

template<class PixelType, std::size_t Dimension>
image<PixelType, Dimension>::image()
    : m_dimensions(helper::make_array<size_t, Dimension>(0)), m_data()
{
    m_data = std::vector<PixelType>(helper::compute_total_size(m_dimensions));
}

template<class PixelType, std::size_t Dimension>
image<PixelType, Dimension>::image(const std::array<std::size_t, Dimension> &a_dimensions)
    : m_dimensions(a_dimensions)
{
    m_data = std::vector<PixelType>(helper::compute_total_size(m_dimensions));
    m_data.resize(helper::compute_total_size(a_dimensions));
}

template<class PixelType, std::size_t Dimension>
image<PixelType, Dimension>::image(std::array<std::size_t, Dimension> &&a_dimensions)
    : m_dimensions(std::move(a_dimensions))
{
    m_data = std::vector<PixelType>(helper::compute_total_size(m_dimensions));
    m_data.resize(helper::compute_total_size(a_dimensions));
}

template<class PixelType, std::size_t Dimension>
image<PixelType, Dimension>::__enable_image_make_shared::__enable_image_make_shared() : image()
{}

template<class PixelType, std::size_t Dimension>
image<PixelType, Dimension>::__enable_image_make_shared::__enable_image_make_shared(
    const std::array<std::size_t, Dimension> &a_dimension)
    : image(a_dimension)
{}

template<class PixelType, std::size_t Dimension>
image<PixelType, Dimension>::__enable_image_make_shared::__enable_image_make_shared(
    std::array<std::size_t, Dimension> &&a_dimension)
    : image(std::move(a_dimension))
{}

template<class PixelType, std::size_t Dimension>
void image<PixelType, Dimension>::resize(const std::array<std::size_t, Dimension> &a_dimensions)
{
    m_dimensions = a_dimensions;
    m_data.resize(helper::compute_total_size(a_dimensions));
}

template<class PixelType, std::size_t Dimension>
void image<PixelType, Dimension>::clear()
{
    if constexpr (!std::is_arithmetic_v<PixelType>)
        memset(bufferPtr()->data(), 0, bufferSizeByte());
    else
        memset(bufferPtr(), 0, bufferSizeByte());
}

template<class PixelType, std::size_t Dimension>
void image<PixelType, Dimension>::fill(const PixelType px)
{
    std::fill(begin(), end(), px);
}

template<class PixelType, std::size_t Dimension>
typename std::vector<PixelType>::iterator image<PixelType, Dimension>::begin()
{
    return m_data.begin();
}

template<class PixelType, std::size_t Dimension>
typename std::vector<PixelType>::iterator image<PixelType, Dimension>::end()
{
    return m_data.end();
}

template<class PixelType, std::size_t Dimension>
typename std::vector<PixelType>::const_iterator image<PixelType, Dimension>::begin() const
{
    return m_data.cbegin();
}

template<class PixelType, std::size_t Dimension>
typename std::vector<PixelType>::const_iterator image<PixelType, Dimension>::end() const
{
    return m_data.cend();
}

template<class PixelType, std::size_t Dimension>
std::shared_ptr<image<PixelType, Dimension>> image<PixelType, Dimension>::create()
{
    return std::make_shared<image<PixelType, Dimension>::__enable_image_make_shared>();
}

template<class PixelType, std::size_t Dimension>
std::shared_ptr<image<PixelType, Dimension>> image<PixelType, Dimension>::create(
    const std::array<std::size_t, Dimension> &a_dimension)
{
    return std::make_shared<image<PixelType, Dimension>::__enable_image_make_shared>(a_dimension);
}

template<class PixelType, std::size_t Dimension>
std::shared_ptr<image<PixelType, Dimension>> image<PixelType, Dimension>::create(
    std::array<std::size_t, Dimension> &&a_dimension)
{
    return std::make_shared<image<PixelType, Dimension>::__enable_image_make_shared>(
        std::move(a_dimension));
}

template<class PixelType, std::size_t Dimension>
const std::array<std::size_t, Dimension> &image<PixelType, Dimension>::dimensions() const
{
    return m_dimensions;
}

} // namespace oni
