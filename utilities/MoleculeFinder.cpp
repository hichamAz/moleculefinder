// CellDetector.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include "img_proc.h"
#include "img_segmentation.h"
#include "img_reader.h"


int main(int argc, char* argv[])
{
    try {

        // Parse arg
        std::string l_filename;
        if (argc > 0) {
            l_filename = std::string(argv[1]);
        }
        else{
            throw std::exception("Please provide a path to tif image");
        }

        // Reading
        auto l_input_images = oni::imread_32F(l_filename);

        // Create Processing Operator
        auto l_proc = std::make_unique<oni::MoleculesFinder>();
        std::vector<std::vector<oni::Rect>> l_res_boxes;
    

        // Do Processing & Get All Rect 
        std::for_each(l_input_images.begin(), l_input_images.end(), [&](const auto& a_input) {
            l_res_boxes.push_back(l_proc->operator()(a_input));
        });

        // Use Opencv To display Rect over original Images
        cv::Mat l_temp;
        cv::Mat l_temp_color;

        for(size_t i(0); i < l_input_images.size(); ++i){

            const std::vector<oni::Rect>& l_vec_rec = l_res_boxes[i];
            l_temp = 10*cv::Mat(l_input_images[i]->height(), l_input_images[i]->width(), CV_32F, l_input_images[i]->bufferPtr());
            l_temp.convertTo(l_temp, CV_8U);
            cv::cvtColor(l_temp, l_temp_color, cv::COLOR_GRAY2RGB);

            for (const auto& l_rect : l_vec_rec) {
               auto l_cv_rect = cv::Rect(cv::Point(l_rect.x, l_rect.y), cv::Point(l_rect.x + l_rect.w, l_rect.y + l_rect.h));
               cv::rectangle(l_temp_color, l_cv_rect.tl(), l_cv_rect.br(), cv::Scalar(0, 0, 255), 1);
            }

            cv::imshow("output", l_temp_color);
        }

        cv::waitKey(0);

    }
    catch (const std::exception& exception) {
        std::cerr << exception.what() << std::endl;
    }

    return 0;
}
