#include "point.h"

namespace oni {

bool operator==(Point const &a, Point const &b)
{
    return (a.x == b.x) && (a.y == b.y);
}

Point operator+(const Point &a, const Point &b)
{
    return Point(a.x + b.x, a.y + b.y);
}

} // namespace oni
