#pragma once

#include <iostream>
#include <algorithm>
#include <functional>
#include <thread>
#include <execution>
#include "image.h"
#include "rect.h"
#include "size.h"

namespace oni {
    // Image processing functions are templated 
    // we should use specialization to improve build time 
    // this functon use multithreading 
    // this is just a demo, we should use a threadPool to avoid thread creation overhead
    // use ctpl.h for example
    template<class PixelType>
    typename image<PixelType, 2>::pointer filter2D(const typename image<PixelType, 2>::pointer a_input, const image<float,2>::pointer a_kernel ) {

        std::vector<std::thread> l_threads;
        constexpr size_t nbThread(8);

        const auto l_widthI = a_input->width();
        const auto l_heightI = a_input->height();
        const int32_t l_centerWK = a_kernel->width() / 2;
        const int32_t l_centerHK = a_kernel->height() / 2;

        auto l_output = image<PixelType>::create(a_input->dimensions());

        // pass by copy so each thread has is own variable
        auto l_func = [=](const size_t a_line_start, const size_t a_line_end) {

            for (int32_t l_rowI(a_line_start); l_rowI < a_line_end; ++l_rowI) {

                for (int32_t l_colI(0); l_colI < l_widthI; ++l_colI) {

                    float l_accum(0);
                    int32_t l_col(0);
                    int32_t l_row(0);

                    for (int32_t l_rowK(-l_centerHK); l_rowK <= l_centerHK; ++l_rowK) {

                        for (int32_t l_colK(-l_centerWK); l_colK <= l_centerWK; ++l_colK) {

                            l_row = std::clamp(int32_t(l_rowI + l_rowK), 0, int32_t(l_heightI) - 1);
                            l_col = std::clamp(int32_t(l_colI + l_colK), 0, int32_t(l_widthI) - 1);
                            l_accum += a_kernel->at(Point(l_colK + l_centerWK, l_rowK + l_centerHK)) * a_input->at(Point(l_col, l_row));
                        }
                    }

                    l_output->at(Point(l_colI, l_rowI)) = l_accum;
                }
            }
        };

        const size_t l_nb_line_per_thread(l_heightI / nbThread);
        size_t l_index(0);

        size_t l_line_start(0);
        size_t l_line_end(0);
        for (size_t i(1); i < nbThread; ++i) {
            l_line_start = (l_index * l_nb_line_per_thread);
            l_line_end = ((l_index+1) * l_nb_line_per_thread);
            l_threads.push_back(std::thread(l_func, l_line_start, l_line_end));
        }
        l_func(l_line_end, a_input->height());

        for (auto& l_it : l_threads) {
            l_it.join();
        }


        return l_output;

    }

    template<class PixelType>
    typename image<PixelType, 2>::pointer erode(const typename image<PixelType, 2>::pointer a_input, const Size& a_size) {

        const auto l_widthI = a_input->width();
        const auto l_heightI = a_input->height();
        const auto l_widthK = a_size.w;
        const auto l_heightK = a_size.h;
        const int32_t l_centerWK = l_widthK / 2;
        const int32_t l_centerHK = l_heightK / 2;

        std::vector<PixelType> l_temp_buffer(l_widthK * l_heightK);
        auto l_output = image<PixelType>::create(a_input->dimensions());

        for (int32_t l_rowI(0); l_rowI < l_heightI; ++l_rowI) {

            for (int32_t l_colI(0); l_colI < l_widthI; ++l_colI) {
                int32_t l_col(0);
                int32_t l_row(0);
                size_t l_buffer_index(0);

                for (int32_t l_rowK(-1*l_centerHK); l_rowK <= l_centerHK; ++l_rowK) {

                    for (int32_t l_colK(-1 * l_centerWK); l_colK <= l_centerWK; ++l_colK) {
                        l_row = std::clamp(int32_t(l_rowI + l_rowK), 0, int32_t(l_heightI - 1));
                        l_col = std::clamp(int32_t(l_colI + l_colK), 0, int32_t(l_widthI - 1));
                        l_temp_buffer[l_buffer_index] = a_input->at(Point(l_col, l_row));
                        ++l_buffer_index;
                    }
                }

                l_output->at(Point(l_colI, l_rowI)) = *std::min_element(l_temp_buffer.begin(), l_temp_buffer.end());;
            }
        }
        return l_output;

    }

    // return a_input1 - a_input2
    template<class PixelType>
    typename image<PixelType, 2>::pointer substract(const typename image<PixelType, 2>::pointer a_input1,
                                                    const typename image<PixelType, 2>::pointer a_input2) {
        // Check dimension in1 & in2
        if(a_input1->dimensions() != a_input2->dimensions()){
            // We should use a map to reference all possible error
            // instead of writing raw string
            throw std::exception("Mismatch dimensions (Input1, Input2) in substract function");
        }
        auto l_output = image<PixelType, 2>::create(a_input1->dimensions());

        // the compiler will use Vectorization AVX/AVX2 + multithreading
        std::transform(std::execution::par_unseq, a_input1->begin(), a_input1->end(), a_input2->begin(), l_output->begin(),
            [](const auto& l_pix1, const auto& l_pix2) // & -> because PixelType could be array ...
            {
                return l_pix1 - l_pix2;
            });
        return l_output;
    }

    template<class PixelType>
    typename image<PixelType, 2>::pointer dilate(const typename image<PixelType, 2>::pointer a_input, const Size& a_size) {

        const auto l_widthI = a_input->width();
        const auto l_heightI = a_input->height();
        const auto l_widthK = a_size.w;
        const auto l_heightK = a_size.h;
        const int32_t l_centerWK = l_widthK / 2;
        const int32_t l_centerHK = l_heightK / 2;

        std::vector<PixelType> l_temp_buffer(l_widthK * l_heightK);
        auto l_output = image<PixelType>::create(a_input->dimensions());

        for (int32_t l_rowI(0); l_rowI < l_heightI; ++l_rowI) {
            for (int32_t l_colI(0); l_colI < l_widthI; ++l_colI) {
                int32_t l_col(0);
                int32_t l_row(0);
                size_t l_buffer_index(0);

                for (int32_t l_rowK(-l_centerHK); l_rowK <= l_centerHK; ++l_rowK) {

                    for (int32_t l_colK(-l_centerWK); l_colK <= l_centerWK; ++l_colK) {

                        l_row = std::clamp(int32_t(l_rowI + l_rowK), 0, int32_t(l_heightI - 1));
                        l_col = std::clamp(int32_t(l_colI + l_colK), 0, int32_t(l_widthI - 1));
                        l_temp_buffer[l_buffer_index] = a_input->at(Point(l_col, l_row));
                        ++l_buffer_index;
                    }
                }

                l_output->at(Point(l_colI, l_rowI)) = *std::max_element(l_temp_buffer.begin(), l_temp_buffer.end());;
            }
        }
        return l_output;

    }

    template<class PixelType>
    void binary_thresold_Inplace(typename image<PixelType>::pointer a_input, const float a_thresold, const float a_min, const float a_max) {
        std::transform(a_input->begin(), a_input->end(), a_input->begin(), [&](auto l_pixel) {
                return (l_pixel > a_thresold) ? a_max : a_min;
            });
    }

    template<class PixelType>
    typename image<PixelType, 2>::pointer blur(const typename image<PixelType, 2>::pointer a_input, const Size& a_size)
    {
        const auto l_kernel = image<float, 2>::create({ {a_size.w, a_size.h} });
        const float l_norm_inv = 1.0f / (a_size.w * a_size.h);
        l_kernel->fill(l_norm_inv);
        return filter2D<PixelType>(a_input, l_kernel);
    }

    template<class PixelType>
    typename image<PixelType, 2>::pointer copyMakeBorder(const typename image<PixelType, 2>::pointer a_input, const size_t a_borderSize, const float a_borderColor) {

       auto l_output = image<PixelType>::create({{a_input->width() + 2 * a_borderSize, a_input->height() + 2 * a_borderSize}});
       l_output->fill(a_borderColor);

       const auto l_input_ptr = a_input->bufferPtr();
       auto l_output_ptr = l_output->bufferPtr();

        for (size_t r(0); r < a_input->height(); ++r) {
            std::copy(l_input_ptr + r * a_input->width(),
                l_input_ptr + (r + 1) * a_input->width(),
                l_output_ptr + (r + a_borderSize) * l_output->width() + a_borderSize);
        }
        return l_output;
    }

}
