#pragma once

#include "point.h"

namespace oni {

struct ONI_ONICORE_EXPORT Rect
{
    Rect(const Point &a_tl, const Point &a_br)
        : x(a_tl.x), y(a_tl.y), w(a_br.x - a_tl.x), h(a_br.y - a_tl.y)
    {}

    void offset(const int32_t a_xOffset, const int32_t a_yOffset){
        x -= a_xOffset;
        y -= a_yOffset;
    }

    size_t x;
    size_t y;
    size_t w;
    size_t h;
};

} // namespace oni
