#pragma once

#include <array>
#include <numeric>
#include <vector>

namespace oni {
namespace helper {

template<class T, size_t N>
std::array<T, N> make_array(const T &v)
{
    std::array<T, N> ret;
    ret.fill(v);
    return ret;
}

template<std::size_t Dimension>
std::size_t compute_total_size(const std::array<std::size_t, Dimension> &a_dimension)
{
    std::size_t l_result = std::accumulate(a_dimension.begin(),
                                           a_dimension.end(),
                                           1,
                                           std::multiplies<std::size_t>());

    return l_result;
}

} // namespace helper

} // namespace oni
