#include "img_reader.h"

namespace oni {

    std::vector<Image32FC1::pointer> imread_32F(const std::string& a_filename) {

        std::vector<cv::Mat> l_input_images;
        std::vector<Image32FC1::pointer> l_output_images;

        cv::imreadmulti(a_filename, l_input_images, cv::IMREAD_GRAYSCALE);
        cv::Mat l_temp;
        
        for (const auto& l_it : l_input_images) {
            l_it.convertTo(l_temp, CV_32F);
            auto l_image = Image32FC1::create({ { (size_t)l_temp.cols, (size_t)l_temp.rows } });
            auto l_ptr = reinterpret_cast<float*>(l_temp.data);
            std::copy(l_ptr, l_ptr + l_image->width() * l_image->height(), l_image->bufferPtr());
            l_output_images.push_back(l_image);
        }

        return l_output_images;
    }

}
