#pragma once

#include "exportmacro.h"
#include <array>
#include <numeric>
#include <vector>

namespace oni {

struct ONI_ONICORE_EXPORT Point
{
    Point(size_t a_x, size_t a_y) : x(a_x), y(a_y) {}
    size_t x;
    size_t y;
};

bool ONI_ONICORE_EXPORT operator==(Point const &a, Point const &b);
Point ONI_ONICORE_EXPORT operator+(const Point &x, const Point &y);

} // namespace oni

namespace std {
template<>
struct hash<oni::Point>
{
    size_t operator()(oni::Point const &a_pt) const noexcept
    {
        std::hash<int> hash_f;
        auto h1 = hash_f(a_pt.x);
        auto h2 = hash_f(a_pt.y);
        return h1 ^ (h2 << 1); // or use boost::hash_combine
    }
};
} // namespace std
