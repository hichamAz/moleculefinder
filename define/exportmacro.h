#ifndef ONI_EXPORT_CONFIG_H
#define ONI_EXPORT_CONFIG_H

#if defined(WIN32)
    #define ONI_DLL_EXPORT __declspec(dllexport)
    #define ONI_DLL_IMPORT __declspec(dllimport)
#else
    #define ONI_DLL_EXPORT
    #define ONI_DLL_IMPORT
#endif

#ifdef ONICore_EXPORTS
#define ONI_ONICORE_EXPORT ONI_DLL_EXPORT
#else
#define ONI_ONICORE_EXPORT ONI_DLL_IMPORT
#endif

#ifdef ImageProcessing_EXPORTS
#define ONI_IMAGEPROCESSING_EXPORT ONI_DLL_EXPORT
#else
#define ONI_IMAGEPROCESSING_EXPORT ONI_DLL_IMPORT
#endif


#endif//ONI_EXPORT_CONFIG_H
