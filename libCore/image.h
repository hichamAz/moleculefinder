#pragma once
#include "exportmacro.h"
#include "image_helper.h"
#include "point.h"
#include <array>
#include <cstdint>
#include <memory>
#include <numeric>
#include <vector>

namespace oni {

class ONI_ONICORE_EXPORT imageBase
{
public:
    using pointer = std::shared_ptr<imageBase>;
    virtual ~imageBase() = default;
    virtual std::size_t width() const = 0;
    virtual std::size_t height() const = 0;
    virtual std::size_t depth() const = 0;
    virtual std::size_t bufferSizeByte() = 0;
    virtual void *rawPtr() = 0;
};

// This is a basic Image class
// Memory is handled by a std::vector
// For more perf we sould use a custom allocator (aligned_memory 64b alignement)
// We sould also use a Memory Pool to avoid alloc/destruction of std::vector overhead

template<class PixelType, std::size_t Dimension = 2>
class image : public imageBase
{
private:
    std::array<std::size_t, Dimension> m_dimensions;
    std::vector<PixelType> m_data;

    image();
    image(std::size_t, std::size_t);
    image(const std::array<std::size_t, Dimension> &);
    image(std::array<std::size_t, Dimension> &&);

    struct __enable_image_make_shared;

public:
    using pointer = std::shared_ptr<image>;
    using pixel_type = PixelType;
    virtual ~image() = default;

    image(const image &) = default;
    image(image &&) = default;

    virtual image &operator=(const image &) = default;
    virtual image &operator=(image &&) = default;

    static std::shared_ptr<image> create();
    static std::shared_ptr<image> create(const std::array<std::size_t, Dimension> &);
    static std::shared_ptr<image> create(std::array<std::size_t, Dimension> &&);

    void resize(const std::array<std::size_t, Dimension> &);

    typename std::vector<PixelType>::iterator begin();
    typename std::vector<PixelType>::iterator end();

    typename std::vector<PixelType>::const_iterator begin() const;
    typename std::vector<PixelType>::const_iterator end() const;

    template<std::size_t Index>
    std::size_t size() const
    {
        static_assert(Index < Dimension, "Index is to out of bound");
        return std::get<Index>(m_dimensions);
    }

    const std::array<std::size_t, Dimension> &dimensions() const;

    std::size_t width() const override final { return m_dimensions[0]; }
    std::size_t height() const override final { return m_dimensions[1]; }
    std::size_t depth() const override final
    {
        if constexpr (Dimension > 2)
            return m_dimensions[2];
        else
            return 1;
    }

    virtual std::size_t bufferSizeByte() override final
    {
        return std::accumulate(m_dimensions.begin(),
                               m_dimensions.end(),
                               1,
                               std::multiplies<std::size_t>())
               * sizeof(pixel_type);
    }

    const PixelType &at(const Point &a_pt) const { return m_data[a_pt.y * width() + a_pt.x]; }

    PixelType &at(const Point &a_pt) { return m_data[a_pt.y * width() + a_pt.x]; }

    size_t step() const { return m_dimensions[0] * sizeof(pixel_type); }
    void clear();
    void fill(const PixelType px);
    std::vector<PixelType> &data() { return m_data; }
    PixelType *bufferPtr() { return m_data.data(); }
    const PixelType *bufferPtr() const { return m_data.data(); }

private:
    virtual void *rawPtr() override final { return this->bufferPtr(); }
};

template<class PixelType, std::size_t Dimension>
struct image<PixelType, Dimension>::__enable_image_make_shared final
    : public image<PixelType, Dimension>
{
    __enable_image_make_shared();
    __enable_image_make_shared(const std::array<std::size_t, Dimension> &);
    __enable_image_make_shared(std::array<std::size_t, Dimension> &&);
};

using Image8UC1 = image<std::uint8_t, 2>;
using Image16UC1 = image<std::uint16_t, 2>;
using Image32FC1 = image<float, 2>;

} // namespace oni
