#pragma once

#include <cstddef>
#include "exportmacro.h"


namespace oni {

struct ONI_ONICORE_EXPORT Size
{
    Size(size_t a_w, size_t a_h) : w(a_w), h(a_h) {}
    size_t w;
    size_t h;
};

} // namespace oni
