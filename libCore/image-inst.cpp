#include "image.hxx"

namespace oni {

    template class ONI_ONICORE_EXPORT image<float, 2>;
    template class ONI_ONICORE_EXPORT image<std::uint8_t, 2>;
    template class ONI_ONICORE_EXPORT image<std::uint16_t, 2>;

}
